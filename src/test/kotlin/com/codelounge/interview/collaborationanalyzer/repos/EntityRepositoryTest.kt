package com.codelounge.interview.collaborationanalyzer.repos

import com.codelounge.interview.collaborationanalyzer.model.Author
import com.codelounge.interview.collaborationanalyzer.model.Branch
import com.codelounge.interview.collaborationanalyzer.model.Project
import com.codelounge.interview.collaborationanalyzer.model.PushEvent
import com.codelounge.interview.collaborationanalyzer.services.RepositoryService
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import java.time.LocalDateTime

@SpringBootTest
class EntityRepositoryTest @Autowired constructor(
    val authorRepository: AuthorRepository,
    val projectRepository: ProjectRepository,
    val branchRepository: BranchRepository,
    val pushEventRepository: PushEventRepository,
    val repositoryService: RepositoryService,
) {
    @BeforeEach
    @AfterAll
    fun clearRepos() {
        repositoryService.clearAll()
    }

    @Nested
    @DisplayName("The author repository")
    inner class AuthorRepositoryTest {

        private val author1 = Author(1, "One")
        private val author2 = Author(2, "Two")

        @Test
        fun `should be initially empty`() {
            assertTrue(authorRepository.findAll().isEmpty(), "Repository should be empty")
        }

        @Test
        fun `should allow adding authors`() {
            authorRepository.save(author1)
            val allAuthors = authorRepository.findAll()
            assertTrue(allAuthors.isNotEmpty(), "Repository should not be empty")
            assertEquals(author1, allAuthors.first(), "Repository should contain added author")
        }

        @Test
        fun `should allow adding multiple authors`() {
            authorRepository.save(author1)
            authorRepository.save(author2)
            val allAuthors = authorRepository.findAll()
            assertEquals(2, allAuthors.size, "Repository should contain two authors")
        }

        @Test
        fun `should overwrite duplicate authors`() {
            authorRepository.save(author1)
            authorRepository.save(author1)
            val allAuthors = authorRepository.findAll()
            assertEquals(1, allAuthors.size, "Repository should contain one author")
        }


        @Test
        fun `should allow finding of added authors by id`() {
            authorRepository.save(author1)
            val foundAuthor = authorRepository.findById(1)
            assertEquals(author1, foundAuthor, "Repository should return added author")
        }

        @Test
        fun `should allow finding of added authors by name`() {
            authorRepository.save(author1)
            val foundAuthor = authorRepository.findByUsername("One")
            assertEquals(author1, foundAuthor, "Repository should return added author")
        }
    }

    @Nested
    @DisplayName("The project repository")
    inner class ProjectRepositoryTest {

        private val project1 = Project(1)
        private val project2 = Project(2)

        @Test
        fun `should be initially empty`() {
            assertTrue(projectRepository.findAll().isEmpty(), "Repository should be empty")
        }

        @Test
        fun `should allow adding projects`() {
            projectRepository.save(project1)
            val allProjects = projectRepository.findAll()
            assertTrue(allProjects.isNotEmpty(), "Repository should not be empty")
            assertEquals(project1, allProjects.first(), "Repository should contain added project")
        }

        @Test
        fun `should allow adding multiple projects`() {
            projectRepository.save(project1)
            projectRepository.save(project2)
            val allProjects = projectRepository.findAll()
            assertEquals(2, allProjects.size, "Repository should contain two projects")
        }

        @Test
        fun `should overwrite duplicate projects`() {
            projectRepository.save(project1)
            projectRepository.save(project1)
            val allProjects = projectRepository.findAll()
            assertEquals(1, allProjects.size, "Repository should contain one project")
        }

        @Test
        fun `should allow finding of added projects by id`() {
            projectRepository.save(project1)
            val foundProject = projectRepository.findById(1)
            assertEquals(project1, foundProject, "Repository should return added project")
        }
    }

    @Nested
    @DisplayName("The branch repository")
    inner class BranchRepositoryTest {

        private val project = Project(1)
        private val branch1 = Branch("1-add-template-code", project)
        private val branch2 = Branch("2-implement-programming-task", project)

        @Test
        fun `should be initially empty`() {
            assertTrue(branchRepository.findAll().isEmpty(), "Repository should be empty")
        }

        @Test
        fun `should allow adding branches`() {
            branchRepository.save(branch1)
            val allBranches = branchRepository.findAll()
            assertTrue(allBranches.isNotEmpty(), "Repository should not be empty")
            assertEquals(branch1, allBranches.first(), "Repository should contain added branch")
        }

        @Test
        fun `should allow adding multiple branches`() {
            branchRepository.save(branch1)
            branchRepository.save(branch2)
            val allBranches = branchRepository.findAll()
            assertEquals(2, allBranches.size, "Repository should contain two branches")
        }

        @Test
        fun `should overwrite duplicate branches`() {
            branchRepository.save(branch1)
            branchRepository.save(branch1)
            val allBranches = branchRepository.findAll()
            assertEquals(1, allBranches.size, "Repository should contain one branch")
        }


        @Test
        fun `should allow finding of added branches by id`() {
            branchRepository.save(branch1)
            val foundBranch = branchRepository.findById(branch1.id) // The id is generates so we don't know it
            assertEquals(branch1, foundBranch, "Repository should return added branch")
        }

        @Test
        fun `should allow finding of added branches by project id and name`() {
            branchRepository.save(branch1)
            val foundBranch = branchRepository.findByProjectIdAndName(1, "1-add-template-code")
            assertEquals(branch1, foundBranch, "Repository should return added branch")
        }
    }

    @Nested
    @DisplayName("The push event repository")
    inner class PushEventRepositoryTest {

        private val project = Project(1)
        private val pushEvent1 = PushEvent(
            1,
            Author(1, "One"),
            Branch("1-add-template-code", project),
            1,
            LocalDateTime.now())
        private val pushEvent2 = PushEvent(
            2,
            Author(2, "Two"),
            Branch("1-add-template-code", project),
            100,
            LocalDateTime.now())

        @Test
        fun `should be initially empty`() {
            assertTrue(pushEventRepository.findAll().isEmpty(), "Repository should be empty")
        }

        @Test
        fun `should allow adding push events`() {
            pushEventRepository.save(pushEvent1)
            val allPushEvents = pushEventRepository.findAll()
            assertTrue(allPushEvents.isNotEmpty(), "Repository should not be empty")
            assertEquals(pushEvent1, allPushEvents.first(), "Repository should contain added push events")
        }

        @Test
        fun `should allow adding multiple push events`() {
            pushEventRepository.save(pushEvent1)
            pushEventRepository.save(pushEvent2)
            val allPushEvents = pushEventRepository.findAll()
            assertEquals(2, allPushEvents.size, "Repository should contain two push events")
        }

        @Test
        fun `should overwrite duplicate push events`() {
            pushEventRepository.save(pushEvent1)
            pushEventRepository.save(pushEvent1)
            val allPushEvents = pushEventRepository.findAll()
            assertEquals(1, allPushEvents.size, "Repository should contain one push event")
        }

        @Test
        fun `should allow finding of added push events by id`() {
            pushEventRepository.save(pushEvent1)
            val foundPushEvent = pushEventRepository.findById(1)
            assertEquals(pushEvent1, foundPushEvent, "Repository should return added push event")
        }
    }
}