package com.codelounge.interview.collaborationanalyzer.model

import com.codelounge.interview.collaborationanalyzer.fixture.PushEventsFixture
import com.codelounge.interview.collaborationanalyzer.services.RepositoryService
import com.codelounge.interview.collaborationanalyzer.services.analysis.EventsAnalysisService
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.Arguments.arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@DisplayName("An author")
class AuthorTest @Autowired constructor(
    private val eventsAnalysisService: EventsAnalysisService,
    private val repositoryService: RepositoryService,
) {

    lateinit var alice: Author
    lateinit var bob: Author
    lateinit var chandler: Author
    lateinit var project1: Project
    lateinit var project2: Project
    lateinit var project3: Project

    @BeforeAll
    fun setUp() {
        PushEventsFixture.ingestSimpleDatasetUsingService(eventsAnalysisService)
        alice = repositoryService.authorByUsername("Alice")!!
        bob = repositoryService.authorByUsername("Bob")!!
        chandler = repositoryService.authorByUsername("Chandler")!!
        project1 = repositoryService.projectById(1)!!
        project2 = repositoryService.projectById(2)!!
        project3 = repositoryService.projectById(3)!!
    }

    @AfterAll
    fun tearDown() {
        repositoryService.clearAll()
    }

    @Nested
    @DisplayName("should have properties with the expected values")
    inner class AuthorPropertyTest {

        @Test
        fun `id = 1`() {
            val expectedId = "1"
            assertEquals(expectedId, alice.id, "The id should be 1")
        }

        @Test
        fun `username = Alice`() {
            val expectedUsername = "Alice"
            assertEquals(expectedUsername, alice.username, "The username should be Alice")
        }

        @Test
        fun `pushEvents size = 3`() {
            val expectedNumberOfPushEvents = 3
            assertEquals(expectedNumberOfPushEvents, alice.pushEvents.size,
                "The number of push events should be 3")
        }

        @Test
        fun `collaborations size = 3`() {
            val expectedNumberOfCollaborations = 3
            assertEquals(expectedNumberOfCollaborations, alice.collaborations.size,
                "The number of collaborations should be 3")
        }

        @Test
        fun `collaborations branches`() {
            val expectedCollaborationBranchNames =
                setOf("1-add-template-code", "2-add-repos")
            assertEquals(expectedCollaborationBranchNames, alice.collaborations.map { it.branch.name }.toSet(),
                "The collaborations should be on the expected branches")
        }

        @Test
        fun `branches size = 3`() {
            val expectedNumberOfBranches = 3
            assertEquals(expectedNumberOfBranches, alice.branches.size,
                "The number of branches should be 3")
        }

        @Test
        fun `branches names`() {
            val expectedBranchNames = setOf("1-add-template-code", "2-add-repos", "1-initial-sprint")
            assertEquals(expectedBranchNames, alice.branches.map { it.name }.toSet(),
                "The branches should have the expected names")
        }

        @Test
        fun `projects size = 2`() {
            val expectedNumberOfProjects = 2
            assertEquals(expectedNumberOfProjects, alice.projects.size,
                "The number of projects should be 2")
        }

        @Test
        fun `projects ids`() {
            val expectedProjectIds = setOf("1", "3")
            assertEquals(expectedProjectIds, alice.projects.map { it.id }.toSet(),
                "The projects should have the expected ids")
        }

    }

    @Nested
    @DisplayName("should have methods that work correctly")
    inner class AuthorMethodTest {

        /**
         * Source for parametrized test, we consider all three projects.
         */
        fun collaboratorsInProjectArgumentsProvider(): List<Arguments> = listOf(
            arguments(project1, setOf(bob, chandler)),
            arguments(project2, emptySet<Author>()),
            arguments(project3, emptySet<Author>()),
        )

        @ParameterizedTest(name = "collaborators in {0}")
        @MethodSource("collaboratorsInProjectArgumentsProvider")
        fun `collaborators of Alice in a project`(project: Project, expectedCollaborators: Set<Author>) {
            val actualCollaborators = alice.collaboratorsInProject(project)
            assertEquals(expectedCollaborators, actualCollaborators,
                "Alice should have ${expectedCollaborators.size} collaborators in $project")
        }

        @Test
        fun `conversion to DTO`() {
            val dto = alice.toDTO()
            assertEquals(alice.id, dto.id, "The id should be the same")
            assertEquals(alice.username, dto.username, "The username should be the same")
        }

        @Test
        fun `conversion to String`() {
            val expectedAliceString = "Alice"
            val actualAliceString = alice.toString()
            assertEquals(expectedAliceString, actualAliceString,
                "The string representation should be \"Alice\"")
        }
    }
}