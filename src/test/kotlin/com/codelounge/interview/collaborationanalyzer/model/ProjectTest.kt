package com.codelounge.interview.collaborationanalyzer.model

import com.codelounge.interview.collaborationanalyzer.fixture.PushEventsFixture
import com.codelounge.interview.collaborationanalyzer.services.RepositoryService
import com.codelounge.interview.collaborationanalyzer.services.analysis.EventsAnalysisService
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.Arguments.arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@DisplayName("A project")
class ProjectTest @Autowired constructor(
    val eventsAnalysisService: EventsAnalysisService,
    val repositoryService: RepositoryService,
) {

    lateinit var project: Project
    lateinit var alice: Author
    lateinit var bob: Author
    lateinit var chandler: Author

    @BeforeAll
    fun setUp() {
        PushEventsFixture.ingestSimpleDatasetUsingService(eventsAnalysisService)
        project = repositoryService.projectById(1)!!
        alice = repositoryService.authorByUsername("Alice")!!
        bob = repositoryService.authorByUsername("Bob")!!
        chandler = repositoryService.authorByUsername("Chandler")!!
    }

    @AfterAll
    fun tearDown() {
        repositoryService.clearAll()
    }

    @Nested
    @DisplayName("should have properties with the expected values")
    inner class ProjectPropertyTest {

        @Test
        fun `id = 1`() {
            val expectedId = "1"
            assertEquals(expectedId, project.id, "The id should be 1")
        }

        @Test
        fun `branches size = 2`() {
            val expectedNumberOfBranches = 2
            assertEquals(expectedNumberOfBranches, project.branches.size,
                "The number of branches should be 2")
        }

        @Test
        fun `branches names`() {
            val expectedBranchNames = setOf("1-add-template-code", "2-add-repos")
            val actualBranchNames = project.branches.map { it.name }.toSet()
            assertEquals(expectedBranchNames, actualBranchNames,
                "The branches should have the expected names")
        }

        @Test
        fun `push events size = 5`() {
            val expectedNumberOfPushEvents = 5
            assertEquals(expectedNumberOfPushEvents, project.pushEvents.size,
                "The number of push events should be 5")
        }

        @Test
        fun `authors size = 3`() {
            val expectedNumberOfAuthors = 3
            assertEquals(expectedNumberOfAuthors, project.authors.size,
                "The number of authors should be 3")
        }

        @Test
        fun `authors = { alice, bob, chandler }`() {
            val expectedAuthors = setOf(alice, bob, chandler)
            val actualAuthors = project.authors
            assertEquals(expectedAuthors, actualAuthors,
                "The authors should be correct")
        }

    }

    @Nested
    @DisplayName("should have methods that work correctly")
    inner class ProjectMethodTest {

        @Test
        fun `collaborations in project`() {
            val expectedNumberOfCollaborations = 4
            val actualNumberOfCollaborations = project.collaborations().size
            assertEquals(expectedNumberOfCollaborations, actualNumberOfCollaborations,
                "The number of collaborations should be 4")
        }

        /**
         * Source for parametrized test, we consider all pairs of authors.
         */
        fun collaborationsOfAuthorsInProjectArgumentsProvider(): List<Arguments> = listOf(
            arguments(alice, bob, 1),
            arguments(alice, chandler, 2),
            arguments(bob, chandler, 1))

        @ParameterizedTest(name = "number of collaborations of {0} and {1} = {2}")
        @MethodSource("collaborationsOfAuthorsInProjectArgumentsProvider")
        fun `collaborations of authors in project 1`(author1: Author, author2: Author, expectedNumberOfCollaborations: Int) {
            val actualNumberOfCollaborations = project.collaborationsBetweenAuthors(author1, author2).size
            assertEquals(expectedNumberOfCollaborations, actualNumberOfCollaborations,
                "$author1 and $author2 should have $expectedNumberOfCollaborations in $project")
        }

        @Test
        fun `conversion to DTO`() {
            val dto = project.toDTO()
            assertEquals(project.id, dto.id, "The id should be the same")
            assertEquals(project.branches.size, dto.numberOfBranches,
                "The number of branches should be the same")
            assertEquals(project.pushEvents.size, dto.numberOfPushEvents,
                "The number of push events should be the same"
            )
        }

        @Test
        fun `conversion to String`() {
            val expectedProjectString = "project 1"
            val actualProjectString = project.toString()
            assertEquals(expectedProjectString, actualProjectString,
                "The string representation should be \"project 1\"")
        }
    }
}