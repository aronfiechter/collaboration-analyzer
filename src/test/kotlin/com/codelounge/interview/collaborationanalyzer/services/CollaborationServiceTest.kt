package com.codelounge.interview.collaborationanalyzer.services

import com.codelounge.interview.collaborationanalyzer.fixture.PushEventsFixture
import com.codelounge.interview.collaborationanalyzer.model.Author
import com.codelounge.interview.collaborationanalyzer.model.Project
import com.codelounge.interview.collaborationanalyzer.services.access.CollaborationService
import com.codelounge.interview.collaborationanalyzer.services.analysis.EventsAnalysisService
import com.codelounge.interview.collaborationanalyzer.utils.DateTime
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.Arguments.arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import java.time.LocalDateTime

@SpringBootTest
@DisplayName("The collaboration service")
class CollaborationServiceTest @Autowired constructor(
    val collaborationService: CollaborationService,
    val eventsAnalysisService: EventsAnalysisService,
    val repositoryService: RepositoryService,
) {
    lateinit var project1: Project
    lateinit var project2: Project
    lateinit var project3: Project

    lateinit var alice: Author
    lateinit var bob: Author
    lateinit var chandler: Author

    @BeforeAll
    fun setUp() {
        PushEventsFixture.ingestSimpleDatasetUsingService(eventsAnalysisService)
        project1 = repositoryService.projectById(1)!!
        project2 = repositoryService.projectById(2)!!
        project3 = repositoryService.projectById(3)!!
        alice = repositoryService.authorByUsername("Alice")!!
        bob = repositoryService.authorByUsername("Bob")!!
        chandler = repositoryService.authorByUsername("Chandler")!!
    }

    @AfterAll
    fun tearDown() {
        repositoryService.clearAll()
    }

    @Nested
    @DisplayName("finds the expected collaborations")
    inner class CollaborationsInProjectTest {

        @Test
        fun `for project 1`() {
            val collaborations = collaborationService.getCollaborationsForProject(project1)
            assertEquals(4, collaborations.size, "Project 1 should have 4 collaborations")

            val expectedCollaborationBranchNames = setOf("1-add-template-code", "2-add-repos")
            val actualCollaborationBranchNames = collaborations.map { it.branch.name }.toSet()
            assertEquals(
                expectedCollaborationBranchNames, actualCollaborationBranchNames,
                "The collaborations should be on the expected branches"
            )

            val expectedCollaborationNames = setOf(
                setOf("Alice", "Bob"),
                setOf("Alice", "Chandler"),
                setOf("Chandler", "Bob")
            )
            val actualCollaborationNames =
                collaborations.map { it.authorsSet().map { author -> author.username }.toSet() }.toSet()
            assertEquals(
                expectedCollaborationNames, actualCollaborationNames,
                "The collaborations should be between the expected authors"
            )
        }

        @Test
        fun `for project 2`() {
            val collaborations = collaborationService.getCollaborationsForProject(project2)
            assertEquals(1, collaborations.size, "Project 2 should have 1 collaborations")

            val expectedCollaborationNames = setOf("Chandler", "Bob")
            val actualCollaborationNames = collaborations.first().authorsSet().map { it.username }.toSet()
            assertEquals(
                expectedCollaborationNames, actualCollaborationNames,
                "The collaborations should be between the expected authors"
            )
        }

        @Test
        fun `for project 3`() {
            // For project 3 there are only two push events on separate branches
            val collaborations = collaborationService.getCollaborationsForProject(project3)
            assertTrue(collaborations.isEmpty(), "Project 3 should have no collaborations")
        }

    }

    @Nested
    @DisplayName("finds the correct collaborators")
    inner class CollaboratorsOfAuthorInProjectTest {

        private val noOne = emptySet<Author>()

        /**
         * Source for parametrized test, all combinations of author and project are considered.
         */
        fun collaboratorsOfAuthorInProjectArgumentsProvider(): List<Arguments> = listOf(
            // Project 1
            arguments(project1, alice, setOf(bob, chandler)),
            arguments(project1, bob, setOf(alice, chandler)),
            arguments(project1, chandler, setOf(alice, bob)),
            // Project 2
            arguments(project2, alice, noOne),
            arguments(project2, bob, setOf(chandler)),
            arguments(project2, chandler, setOf(bob)),
            // Project 3
            arguments(project3, alice, noOne),
            arguments(project3, bob, noOne),
            arguments(project3, chandler, noOne),
        )

        @ParameterizedTest(name = "for {1} in {0}")
        @MethodSource("collaboratorsOfAuthorInProjectArgumentsProvider")
        fun `for an author in a project`(project: Project, author: Author, expectedCollaborators: Set<Author>) {
            val collaborators = collaborationService.getCollaboratorsForAuthorInProject(author, project)
            assertEquals(expectedCollaborators, collaborators,
                "The collaborators of $author in $project should be $expectedCollaborators"
            )
        }
    }

    @Nested
    @DisplayName("finds the correct collaborations")
    inner class CollaborationsOfAuthorsInProjectTest {

        inner class ExpectedCollaborationData(
            val branchName: String,
            val numberOfCommits: Int,
            val startTimestamp: LocalDateTime,
            val endTimestamp: LocalDateTime
        )

        /**
         * Source for parametrized test, all combinations of pairs of authors in a project are considered.
         */
        fun collaborationsOfAuthorsInProjectArgumentsProvider(): List<Arguments> = listOf(

            // Project 1 - everyone collaborated with everyone, Alice and Chandler twice
            arguments(project1, alice, bob, 1, listOf(
                    ExpectedCollaborationData(
                        "1-add-template-code",
                        7,
                        DateTime.parse("2021-06-01T00:00:00.000Z"),
                        DateTime.parse("2021-06-02T00:00:00.000Z")))),
            arguments(project1, alice, chandler, 2, listOf(
                    ExpectedCollaborationData(
                        "1-add-template-code",
                        6,
                        DateTime.parse("2021-06-01T00:00:00.000Z"),
                        DateTime.parse("2021-06-03T00:00:00.000Z")),
                    ExpectedCollaborationData(
                        "2-add-repos",
                        30,
                        DateTime.parse("2021-06-04T00:00:00.000Z"),
                        DateTime.parse("2021-06-05T00:00:00.000Z")))),
            arguments(project1, bob, chandler, 1, listOf(
                    ExpectedCollaborationData(
                        "1-add-template-code",
                        5,
                        DateTime.parse("2021-06-02T00:00:00.000Z"),
                        DateTime.parse("2021-06-03T00:00:00.000Z")))),

            // Project 2 - only Bob and Chandler collaborated
            arguments(project2, alice, bob, 0, emptyList<ExpectedCollaborationData>()),
            arguments(project2, alice, chandler, 0, emptyList<ExpectedCollaborationData>()),
            arguments(project2, bob, chandler, 1, listOf(
                    ExpectedCollaborationData(
                        "1-initial-sprint",
                        27,
                        DateTime.parse("2021-06-06T00:00:00.000Z"),
                        DateTime.parse("2021-06-07T00:00:00.000Z")))),

            // Project 3 - no collaborations
            arguments(project3, alice, bob, 0, emptyList<ExpectedCollaborationData>()),
            arguments(project3, alice, chandler, 0, emptyList<ExpectedCollaborationData>()),
            arguments(project3, bob, chandler, 0, emptyList<ExpectedCollaborationData>()),
        )

        @ParameterizedTest(name = "for {1} and {2} in {0}")
        @MethodSource("collaborationsOfAuthorsInProjectArgumentsProvider")
        fun `for two authors in a project`(
            project: Project,
            author1: Author,
            author2: Author,
            expectedNumberOfCollaborations: Int,
            expectedCollaborationsData: List<ExpectedCollaborationData>
        ) {
            val actualCollaborations = collaborationService
                .getCollaborationsBetweenAuthorsInProject(author1, author2, project)
                .toList()
            val theAuthors = "${author1.username} and ${author2.username}"

            assertEquals(expectedNumberOfCollaborations, actualCollaborations.size,
                "$theAuthors should have $expectedNumberOfCollaborations collaboration(s) on project ${project.id}")
            actualCollaborations.sortedBy { it.branch.name }.forEachIndexed { i, actual ->
                val expected = expectedCollaborationsData[i]

                assertEquals(expected.branchName, actual.branch.name,
                    "The collaboration should be on branch ${expected.branchName}")
                assertEquals(expected.numberOfCommits, actual.numberOfCommits,
                    "The collaboration should have ${expected.numberOfCommits} total commits")
                assertEquals(expected.startTimestamp, actual.startTimestamp,
                    "The collaboration should have started at ${expected.startTimestamp}")
                assertEquals(expected.endTimestamp, actual.endTimestamp,
                    "The collaboration should have ended at ${expected.endTimestamp}")
            }
        }
    }
}
