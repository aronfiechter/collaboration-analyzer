package com.codelounge.interview.collaborationanalyzer.services.analysis

import com.codelounge.interview.collaborationanalyzer.model.collaboration.Collaboration
import com.codelounge.interview.collaborationanalyzer.model.PushEvent
import org.springframework.stereotype.Service

@Service
class CollaborationAnalysisService {

    /**
     * Given push events and a regex to exclude branch names from the analysis,
     * create all collaborations.
     * A collaboration happens when two different authors push some commits on the same branch.
     */
    fun extractCollaborations(
        pushEvents: List<PushEvent>,
        excludedBranchNamesRegex: String,
    ): List<Collaboration> {
        return pushEvents
            .map { it.branch }
            .distinct()
            .filterNot { it.name.matches(excludedBranchNamesRegex.toRegex()) }
            .flatMap { Collaboration.createCollaborationsForBranch(it) }
    }

    /**
     * Reset all found collaborations.
     * Since the collaborations are stored in the authors, we clear them there.
     */
    fun resetCollaborations(pushEvents: List<PushEvent>) {
        pushEvents.forEach { it.author.collaborations.clear() }
    }
}