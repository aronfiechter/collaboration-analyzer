package com.codelounge.interview.collaborationanalyzer.services.analysis

import com.codelounge.interview.collaborationanalyzer.controllers.dto.InputEventDTO
import com.codelounge.interview.collaborationanalyzer.controllers.dto.InputPushEventDTO
import org.springframework.stereotype.Service

@Service
class EventsPreprocessingService {

    /**
     * Filter the raw GitLab events to only keep push events
     * of the kind "pushed to" or "pushed new".
     *
     * @param inputEventDTOs raw input events
     * @return push events
     */
    fun cleanAndExtractPushEvents(inputEventDTOs: List<InputEventDTO>): List<InputPushEventDTO> =
        inputEventDTOs
            .filter { it.isPushedOrCreatedEvent() }
            .distinctBy { it.id }
            .map { it.asInputPushEventDTO() }

}