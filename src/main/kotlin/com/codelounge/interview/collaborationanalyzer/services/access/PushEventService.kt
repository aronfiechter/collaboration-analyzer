package com.codelounge.interview.collaborationanalyzer.services.access

import com.codelounge.interview.collaborationanalyzer.model.PushEvent
import com.codelounge.interview.collaborationanalyzer.services.RepositoryService
import org.springframework.stereotype.Service

@Service
class PushEventService(private val repositoryService: RepositoryService) {

    /**
     * Get all PushEvents.
     */
    fun getAll(): List<PushEvent> = repositoryService.pushEvents()
}
