package com.codelounge.interview.collaborationanalyzer.services.analysis

import com.codelounge.interview.collaborationanalyzer.controllers.dto.InputPushEventDTO
import com.codelounge.interview.collaborationanalyzer.model.Author
import com.codelounge.interview.collaborationanalyzer.model.Branch
import com.codelounge.interview.collaborationanalyzer.model.Project
import com.codelounge.interview.collaborationanalyzer.model.PushEvent
import com.codelounge.interview.collaborationanalyzer.services.RepositoryService
import com.codelounge.interview.collaborationanalyzer.utils.DateTime
import org.springframework.stereotype.Service

@Service
class EventsStoringService(private val repositoryService: RepositoryService) {

    /**
     * Store all push events and related entities (projects, branches, authors).
     */
    fun storePushEvents(inputPushEventDTOSs: List<InputPushEventDTO>): List<PushEvent> {
        storeProjectsAuthorsAndBranches(inputPushEventDTOSs)
        return storeAndLinkPushEvents(inputPushEventDTOSs)
    }

    /**
     * Store all related entities (projects, branches, authors).
     */
    private fun storeProjectsAuthorsAndBranches(inputPushEventDTOs: List<InputPushEventDTO>) {
        repositoryService.storeAll(inputPushEventDTOs
            .distinctBy { it.project_id }
            .map { Project(it.project_id) })
        repositoryService.storeAll(inputPushEventDTOs
            .distinctBy { it.author_id }
            .map { Author(it.author_id, it.author_username) })
        repositoryService.storeAll(inputPushEventDTOs
            .distinctBy { "${it.project_id}-${it.push_data.ref}" }
            .map { Branch(it.push_data.ref, repositoryService.projectById(it.project_id)!!) })
    }

    /**
     * Store all push events, linking them to the related entities.
     */
    private fun storeAndLinkPushEvents(inputPushEventDTOs: List<InputPushEventDTO>): List<PushEvent> {
        val pushEvents = repositoryService.storeAll(inputPushEventDTOs
            .map { PushEvent(
                id = it.id,
                author = repositoryService.authorById("${it.author_id}")!!,
                branch = repositoryService.branchByProjectIdAndName(it.project_id, it.push_data.ref)!!,
                numberOfCommits = it.push_data.commit_count,
                timestamp = DateTime.parse(it.created_at))
            })
        pushEvents.forEach {
            it.branch.addPushEvent(it)
            it.author.addPushEvent(it)
        }
        return pushEvents
    }
}