package com.codelounge.interview.collaborationanalyzer.services.access

import com.codelounge.interview.collaborationanalyzer.model.Author
import com.codelounge.interview.collaborationanalyzer.model.collaboration.Collaboration
import com.codelounge.interview.collaborationanalyzer.model.Project
import com.codelounge.interview.collaborationanalyzer.services.RepositoryService
import org.springframework.stereotype.Service

@Service
class CollaborationService {

    /**
     * Get all collaborations in a Project.
     */
    fun getCollaborationsForProject(project: Project): Set<Collaboration> =
        project.collaborations()

    /**
     * Get all collaborators of an Author in a Project.
     */
    fun getCollaboratorsForAuthorInProject(author: Author, project: Project): Set<Author> =
        author.collaboratorsInProject(project)

    /**
     * Get all collaborations of two Authors in a Project.
     */
    fun getCollaborationsBetweenAuthorsInProject(author1: Author, author2: Author, project: Project): Set<Collaboration> =
        project.collaborationsBetweenAuthors(author1, author2)
}
