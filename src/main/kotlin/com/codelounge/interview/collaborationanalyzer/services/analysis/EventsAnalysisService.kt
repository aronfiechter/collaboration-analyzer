package com.codelounge.interview.collaborationanalyzer.services.analysis

import com.codelounge.interview.collaborationanalyzer.configuration.CollaborationAnalyzerProperties
import com.codelounge.interview.collaborationanalyzer.controllers.dto.AnalysisSettingsDTO
import com.codelounge.interview.collaborationanalyzer.controllers.dto.InputEventDTO
import com.codelounge.interview.collaborationanalyzer.model.collaboration.Collaboration
import com.codelounge.interview.collaborationanalyzer.services.RepositoryService
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class EventsAnalysisService(
    private val eventsPreprocessingService: EventsPreprocessingService,
    private val eventsStoringService: EventsStoringService,
    private val collaborationAnalysisService: CollaborationAnalysisService,
    private val repositoryService: RepositoryService,
    private val properties: CollaborationAnalyzerProperties,
) {

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Ingest and analyse raw events to find collaborations.
     * The following steps are performed:
     * - clean the input data and keep only push events
     * - store all push events and related data (projects, branches, authors)
     * - perform analysis to find collaborations (using provided settings)
     *
     * @param inputEventDTOs raw input events
     * @param analysisSettingsDTO analysis setting
     * @return the found collaborations
     */
    fun ingestAndAnalyseEvents(
        inputEventDTOs: List<InputEventDTO>,
        analysisSettingsDTO: AnalysisSettingsDTO = AnalysisSettingsDTO(properties.excludeBranchesMatching),
    ): List<Collaboration> {
        log.info("Received ${inputEventDTOs.size} events")
        val inputPushEventDTOs = eventsPreprocessingService.cleanAndExtractPushEvents(inputEventDTOs)
        val pushEvents = eventsStoringService.storePushEvents(inputPushEventDTOs)
        val collaborations = collaborationAnalysisService
            .extractCollaborations(pushEvents, analysisSettingsDTO.excludedBranchNamesRegex)
        log.info("Found ${pushEvents.size} push events and ${collaborations.size} collaborations")
        return collaborations
    }

    /**
     * Repeat the analysis on the ingested data.
     * The following steps are performed:
     * - get all existing push events from the repository
     * - reset all found collaborations
     * - perform analysis to find collaborations (using provided settings)
     *
     * @param analysisSettingsDTO analysis setting
     * @return the found collaborations
     */
    fun repeatAnalysis(analysisSettingsDTO: AnalysisSettingsDTO): List<Collaboration> {
        log.info("Repeating analysis excluding branches matching: ${analysisSettingsDTO.excludedBranchNamesRegex}")
        val pushEvents = repositoryService.pushEvents()
        collaborationAnalysisService.resetCollaborations(pushEvents)
        val collaborations = collaborationAnalysisService
            .extractCollaborations(pushEvents, analysisSettingsDTO.excludedBranchNamesRegex)
        log.info("Found ${collaborations.size} collaborations")
        return collaborations
    }
}
