package com.codelounge.interview.collaborationanalyzer.controllers.dto

/**
 * An event from the GitLab events API.
 * This structure is based on the test data that can be found in `src/test/resources/data`.
 */
data class InputEventDTO(
    val id: Int,
    val project_id: Int,
    val action_name: String,
    val author_id: Int,
    val created_at: String,
    val push_data: PushDataDTO?,
    val author_username: String,
) {
    private fun isPushedToBranchEvent() = push_data?.ref_type == "branch" && push_data.action == "pushed"
    private fun isCreatedBranchEvent() = push_data?.ref_type == "branch" && push_data.action == "created"
    fun isPushedOrCreatedEvent() = isPushedToBranchEvent() || isCreatedBranchEvent()

    fun asInputPushEventDTO() = InputPushEventDTO.from(this)
}

/**
 * A push event event from the GitLab events API.
 * This is the same as the above class, but with non-nullable push_data field.
 */
data class InputPushEventDTO(
    val id: Int,
    val project_id: Int,
    val author_id: Int,
    val created_at: String,
    val push_data: PushDataDTO,
    val author_username: String,
) {
    companion object {
        fun from(inputEventDTO: InputEventDTO): InputPushEventDTO = InputPushEventDTO(
            inputEventDTO.id,
            inputEventDTO.project_id,
            inputEventDTO.author_id,
            inputEventDTO.created_at,
            inputEventDTO.push_data!!,
            inputEventDTO.author_username)
    }
}

/**
 * The push_data section of the push events of the GitLab API.
 */
data class PushDataDTO(
    val commit_count: Int,
    val action: String,
    val ref_type: String,
    val commit_from: String?,
    val commit_to: String?,
    val ref: String,
    val commit_title: String?,
)
