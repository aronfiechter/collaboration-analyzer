package com.codelounge.interview.collaborationanalyzer.controllers

import com.codelounge.interview.collaborationanalyzer.model.AuthorDTO
import com.codelounge.interview.collaborationanalyzer.model.collaboration.CollaborationAuthorsDTO
import com.codelounge.interview.collaborationanalyzer.model.collaboration.CollaborationDTO
import com.codelounge.interview.collaborationanalyzer.services.RepositoryService
import com.codelounge.interview.collaborationanalyzer.services.access.CollaborationService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/collaborations")
class CollaborationController(
    val repositoryService: RepositoryService,
    val collaborationService: CollaborationService,
) {

    /**
     * Handle a nullable result to build a response entity.
     * If the given value is null, return a "Not Found" response,
     * otherwise return 200 OK with the result.
     */
    private fun <T> handleNotFound(result: T?): ResponseEntity<T> = when (result) {
        null -> ResponseEntity.notFound().build()
        else -> ResponseEntity.ok(result)
    }

    /**
     * Given a project id,
     * get all the distinct pairs of authors that collaborated on the project.
     *
     * @param projectId the id of a project
     * @return distinct pairs of authors
     */
    @GetMapping("/project/{projectId}/pairs")
    fun getCollaborationPairsForProject(@PathVariable projectId: Int): ResponseEntity<Set<CollaborationAuthorsDTO>> {
        val result = repositoryService.projectById(projectId)
            ?.let {
                collaborationService.getCollaborationsForProject(it)
                    .map { collaboration -> collaboration.toAuthorsDTO() }
                    .toSet()
            }
        return handleNotFound(result)
    }

    /**
     * Given an author username and a project id,
     * get all the other authors that collaborated with them on the project.
     *
     * @param projectId the id of a project
     * @param authorUsername the username of an author
     * @return set of collaborators of the author
     */
    @GetMapping("/project/{projectId}/collaborators/{authorUsername}")
    fun getCollaboratorsOfAuthorInProject(
        @PathVariable projectId: Int,
        @PathVariable authorUsername: String,
    ): ResponseEntity<Set<AuthorDTO>> {
        val result = repositoryService.projectById(projectId)
            ?.let { project ->
                repositoryService.authorByUsername(authorUsername)
                    ?.let { author ->
                        collaborationService.getCollaboratorsForAuthorInProject(author, project)
                            .map { it.toDTO() }
                            .toSet()
                    }
            }
        return handleNotFound(result)
    }

    /**
     * Given a pair of author (by username) and a project id,
     * get all the branch names where the authors collaborated, along with the beginning and end
     * of the collaboration in the branch, and the total number of commits.
     *
     * @param projectId the id of a project
     * @param author1Username the username of an author
     * @param author2Username the username of another author
     * @return set of collaborations of the two authors in the project
     */
    @GetMapping("/project/{projectId}/author1/{author1Username}/author2/{author2Username}")
    fun getCollaborationsOfAuthorsInProject(
        @PathVariable projectId: Int,
        @PathVariable author1Username: String,
        @PathVariable author2Username: String,
    ): ResponseEntity<Set<CollaborationDTO>> {
        val result = repositoryService.projectById(projectId)
            ?.let { project ->
                repositoryService.authorByUsername(author1Username)
                    ?.let { author1 ->
                        repositoryService.authorByUsername(author2Username)
                            ?.let { author2 ->
                                collaborationService
                                    .getCollaborationsBetweenAuthorsInProject(author1, author2, project)
                                    .map { it.toFullDTO() }
                                    .toSet()
                            }
                    }
            }
        return handleNotFound(result)
    }
}
