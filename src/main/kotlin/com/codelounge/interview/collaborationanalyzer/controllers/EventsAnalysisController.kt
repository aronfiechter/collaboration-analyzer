package com.codelounge.interview.collaborationanalyzer.controllers

import com.codelounge.interview.collaborationanalyzer.configuration.CollaborationAnalyzerProperties
import com.codelounge.interview.collaborationanalyzer.controllers.dto.AnalysisSettingsDTO
import com.codelounge.interview.collaborationanalyzer.controllers.dto.InputEventDTO
import com.codelounge.interview.collaborationanalyzer.model.PushEventDTO
import com.codelounge.interview.collaborationanalyzer.model.collaboration.CollaborationDTO
import com.codelounge.interview.collaborationanalyzer.services.analysis.EventsAnalysisService
import com.codelounge.interview.collaborationanalyzer.services.access.PushEventService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/events")
class EventsAnalysisController(
    private val eventsAnalysisService: EventsAnalysisService,
    private val pushEventService: PushEventService,
    private val properties: CollaborationAnalyzerProperties,
) {

    /**
     * Given events of the GitLab API in JSON format,
     * ingest the relevant data and perform a collaboration analysis.
     *
     * @param inputEventDTOs the raw input events.
     * @return the found collaborations
     */
    @PostMapping
    fun postEvents(@RequestBody inputEventDTOs: List<InputEventDTO>): List<CollaborationDTO> {
        val collaborations = eventsAnalysisService.ingestAndAnalyseEvents(inputEventDTOs)
        return collaborations.map { it.toFullDTO() }
    }

    /**
     * Given analysis settings,
     * scrap all found collaborations and repeat the collaboration analysis.
     *
     * @param analysisSettingsDTO collaboration analysis settings
     * @return the found collaborations
     */
    @PostMapping("/repeat_analysis")
    fun repeatAnalysis(
        @RequestBody analysisSettingsDTO: AnalysisSettingsDTO?,
    ): List<CollaborationDTO> {
        val settings = analysisSettingsDTO ?: AnalysisSettingsDTO(properties.excludeBranchesMatching)
        val collaborations = eventsAnalysisService.repeatAnalysis(settings)
        return collaborations.map { it.toFullDTO() }
    }

    /**
     * Get all ingested push events.
     *
     * @return list of push events
     */
    @GetMapping
    fun getAllEvents(): List<PushEventDTO> {
        return pushEventService.getAll().map { it.toDTO() }
    }

}
