package com.codelounge.interview.collaborationanalyzer.repos

import com.codelounge.interview.collaborationanalyzer.model.*
import org.springframework.stereotype.Repository

/**
 * An abstract repository for Entities.
 */
sealed class EntityRepository<T : Entity> {
    private val entitiesById = mutableMapOf<String, T>()

    /**
     * Find all entities in the repository.
     */
    fun findAll(): List<T> = entitiesById.values.toList()

    /**
     * Find an entity by its id.
     */
    fun findById(id: String) = entitiesById[id]

    /**
     * Save a new entity.
     */
    fun save(entity: T) = entitiesById.put(entity.id, entity)

    /**
     * Remove all entities stored in the repository.
     */
    fun clear() = entitiesById.clear()

}

@Repository
class AuthorRepository : EntityRepository<Author>() {
    fun findById(id: Int) = findById("$id")

    /**
     * Find an Author by username.
     */
    fun findByUsername(username: String): Author? = findAll().find { it.username == username }
}

@Repository
class ProjectRepository : EntityRepository<Project>() {
    fun findById(id: Int) = findById("$id")
}

@Repository
class BranchRepository : EntityRepository<Branch>() {

    /**
     * Find a Branch by project id and branch name.
     */
    fun findByProjectIdAndName(projectId: Int, name: String) =
        findById("$projectId-$name")
}

@Repository
class PushEventRepository : EntityRepository<PushEvent>() {
    fun findById(id: Int) = findById("$id")
}
