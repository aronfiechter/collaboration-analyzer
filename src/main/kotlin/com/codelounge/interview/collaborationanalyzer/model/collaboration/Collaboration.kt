package com.codelounge.interview.collaborationanalyzer.model.collaboration

import com.codelounge.interview.collaborationanalyzer.model.*

/**
 * A collaboration happens between two authors on a branch.
 */
class Collaboration(
    val author1: Author,
    val author2: Author,
    val branch: Branch,
) {

    /**
     * When a Collaboration is created, add it to both involved Authors.
     */
    init {
        author1.addCollaboration(this)
        author2.addCollaboration(this)
    }

    companion object Factory {

        /**
         * Create all collaborations on a branch.
         */
        fun createCollaborationsForBranch(branch: Branch): List<Collaboration> =
            branch.collaboratorPairs().map {
                val authors = it.toList()
                Collaboration(authors[0], authors[1], branch) }
    }

    /* Lazy properties */

    val project by lazy { branch.project }
    private val pushEvents by lazy { branch.pushEvents.filter { e ->
        e.author == author1 || e.author == author2 }.toSet() }
    val numberOfCommits by lazy { pushEvents.sumOf { it.numberOfCommits } }
    val startTimestamp by lazy { pushEvents.minOf { it.timestamp } }
    val endTimestamp by lazy { pushEvents.maxOf { it.timestamp } }

    /**
     * Given one of the authors in this collaboration, returns the other author.
     */
    fun collaboratorOf(author: Author): Author {
        require(author == author1 || author == author2)
        return if (author == author1) author2 else author1
    }

    /* Support functions for equals and hashCode */

    fun authorsSet(): Set<Author> = setOf(author1, author2)
    private fun sameAuthorsAs(other: Collaboration) = other.authorsSet() == this.authorsSet()

    /**
     * We consider two collaborations to be the same if they have the
     * same properties (ignoring the order of authors).
     */
    override fun equals(other: Any?): Boolean {
        return other is Collaboration
                && (sameAuthorsAs(other) && other.branch == this.branch)
    }

    /**
     * For the HashCode, we sum the hashCodes of the two authors
     * so that order does not matter.
     */
    override fun hashCode(): Int {
        val combinedAuthorsHashCode = author1.hashCode() + author2.hashCode()
        return  31 * combinedAuthorsHashCode + branch.hashCode()
    }

    /* Conversions to DTOs */

    fun toFullDTO(): CollaborationDTO =
        CollaborationDTO(
            author1.toDTO(),
            author2.toDTO(),
            branch.name,
            project.id,
            numberOfCommits,
            startTimestamp = startTimestamp.toString(),
            endTimestamp = endTimestamp.toString())

    fun toAuthorsDTO(): CollaborationAuthorsDTO =
        CollaborationAuthorsDTO(authorsSet().map { it.toDTO() }.toSet())

}

/**
 * This DTO stores all data relevant for a collaboration between two authors on a branch.
 */
data class CollaborationDTO(
    val author1: AuthorDTO,
    val author2: AuthorDTO,
    val branchName: String,
    val projectId: String,
    val numberOfCommits: Int,
    val startTimestamp: String,
    val endTimestamp: String,
)

/**
 * This DTO only stores the two authors in a collaboration.
 */
data class CollaborationAuthorsDTO(
    val authors: Set<AuthorDTO>,
)
