package com.codelounge.interview.collaborationanalyzer.model

/**
 * A branch has a name and a project.
 * It has a set of push events that happened on it.
 */
class Branch(
    val name: String,
    val project: Project,
    val pushEvents: MutableSet<PushEvent> = mutableSetOf(),
) : Entity("${project.id}-$name") {

    /**
     * When a Branch is created, add it to the containing Project.
     */
    init {
        project.addBranch(this)
    }

    /* Lazy properties */

    val authors by lazy { pushEvents.map { it.author }.toSet() }

    /* Method to add push events */

    fun addPushEvent(pushEvent: PushEvent) = this.pushEvents.add(pushEvent)

    /**
     * Get the set of all pairs of authors that authored push events on this branch.
     */
    fun collaboratorPairs(): Set<Set<Author>> =
        authors
            .flatMap { a1 -> authors.minus(a1).map { a2 -> setOf(a1, a2) } }
            .toSet()

    /* Conversion to DTO */

    override fun toDTO(): BranchDTO = BranchDTO(id, name, project.toDTO(), pushEvents.size)

}

data class BranchDTO(
    val id: String,
    val name: String,
    val project: ProjectDTO,
    val numberOfPushEvents: Int,
) : EntityDTO()
