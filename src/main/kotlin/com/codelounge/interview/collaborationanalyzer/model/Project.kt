package com.codelounge.interview.collaborationanalyzer.model

import com.codelounge.interview.collaborationanalyzer.model.collaboration.Collaboration

/**
 * A project has an id.
 * It has list of branches that are part of it.
 */
class Project(
    id: Int,
    val branches: MutableSet<Branch> = mutableSetOf(),
) : Entity("$id") {

    /* Lazy properties */

    val pushEvents by lazy { branches.flatMap { it.pushEvents }.toSet() }
    val authors by lazy { branches.flatMap { it.authors }.toSet() }

    /* Method to add branches */

    fun addBranch(branch: Branch) = this.branches.add(branch)

    /**
     * Get all collaborations that happened on this project.
     */
    fun collaborations(): Set<Collaboration> =
        authors
            .flatMap { it.collaborations }
            .filter { it.project == this }
            .toSet()

    /**
     * Get all collaborations between two given authors that happened on this project.
     */
    fun collaborationsBetweenAuthors(author1: Author, author2: Author): Set<Collaboration> =
        collaborations()
            .filter { it.authorsSet() == setOf(author1, author2) }
            .toSet()

    /* Conversion to DTO and to String */

    override fun toDTO(): ProjectDTO = ProjectDTO(id, branches.size, pushEvents.size)
    override fun toString(): String = "project $id"
}

data class ProjectDTO(
    val id: String,
    val numberOfBranches: Int,
    val numberOfPushEvents: Int,
) : EntityDTO()
