package com.codelounge.interview.collaborationanalyzer.model

import com.codelounge.interview.collaborationanalyzer.model.collaboration.Collaboration

/**
 * An author has an ID and a username.
 * They have a set of push events they authored,
 * and a set of collaborations they participated in.
 */
class Author(
    id: Int,
    val username: String,
    val pushEvents: MutableSet<PushEvent> = mutableSetOf(),
    val collaborations: MutableSet<Collaboration> = mutableSetOf(),
) : Entity("$id") {

    /* Lazy properties */

    val branches by lazy { pushEvents.map { it.branch }.toSet() }
    val projects by lazy { pushEvents.map { it.project }.toSet() }

    /* Methods to add push events and collaborations */

    fun addPushEvent(pushEvent: PushEvent) = pushEvents.add(pushEvent)
    fun addCollaboration(collaboration: Collaboration) = collaborations.add(collaboration)

    /**
     * Get all collaborations of this author in a specific project.
     */
    private fun collaborationsInProject(project: Project): Set<Collaboration> =
        collaborations
            .filter { it.project == project }
            .toSet()

    /**
     * Get all authors that collaborated with this author in a specific project.
     */
    fun collaboratorsInProject(project: Project): Set<Author> =
        collaborationsInProject(project)
            .map { it.collaboratorOf(this) }
            .toSet()

    /* Conversion to DTO and to String */

    override fun toDTO(): AuthorDTO = AuthorDTO(id, username)
    override fun toString(): String = username
}

data class AuthorDTO(
    val id: String,
    val username: String,
) : EntityDTO()
