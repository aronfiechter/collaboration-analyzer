[![pipeline status](https://gitlab.com/aron.fiechter/collaboration-analyzer/badges/main/pipeline.svg)](https://gitlab.com/aron.fiechter/collaboration-analyzer/-/commits/main)
[![coverage report](https://gitlab.com/aron.fiechter/collaboration-analyzer/badges/main/coverage.svg)](https://gitlab.com/aron.fiechter/collaboration-analyzer/-/commits/main)

# Collaboration Analyzer

The Collaboration Analyzer (CA) analyzes GitLab events to find collaborations: A collaboration happens when two different 
authors push some commits on the same branch.

The analysis allows to exclude a configurable set of branches that match a regex, e.g. `dev.*` or `main | master`.

After the analysis, the CA provides the following functionalities:

- Given a project id, return all the distinct pair of authors that collaborated on the project;
- Given an author and a project id, return all the other authors that collaborated with him or her on the project;
- Given a pair of authors and a project id, return all the branch names where the authors collaborated. The list includes also the beginning and end of the collaboration in the branch, and the total number of commits.

## Assumptions

For the analysis, we assume the following things:

- We consider author usernames to be unique.
- We consider only events with `action_name` equal to `"pushed to"` or `"pushed new"`, as these are the only kinds of
  events in the provided dataset that had non-zero `commit_count`s.
- We ignore `removed` events because, even though they are pertaining to branches, they always have `commit_count: 0`.
- In the case of a merge of a source branch into a target branch, we only consider the author of the event to be a
  collaborator of the target branch. We do not "merge" the collaborators of the source branch into the set of
  collaborators of the target branch.

## How to run

The CA is implemented as a simple REST API written in Kotlin using Spring Boot.  
For the build process we use Gradle.

The application needs Java 11 installed (openjdk 11.0.10 2021-01-19).

- Run `./gradlew kaptKotlin` to configure the annotation processor for Kotlin.
- Compile with: `./gradlew assemble`.
- Run with: `./gradlew bootRun`.

The API is available at `localhost:8080`.  
There is no persistence layer, or rather, the repositories persist data in memory, so all analysed data is lost
when the application stops.

You can also import the project in IntelliJ (works on IntelliJ IDEA 2021.1.2 (Ultimate Edition)).

### Configuration

The configuration for the application can be found in `src/main/kotlin/resources/applications.properties`.

We support one setting, `analysis.exclude-branches-matching`, which represents the default regex that is used to
exclude branches from the analysis.

## API

The REST API offers two endpoints: `/api/events` and `/api/collaborations`.

### Events endpoint `/api/events`

#### POST `/api/events`

Expects a list of events, ingests all the data, and performs a collaboration analysis with the default settings.  
Returns a list of the found collaborations.

Input body format: a JSON like the ones in `src/test/resources/data`.  
Response format: a list of CollaborationDTO objects.

#### GET `/api/events`

Returns a list of all ingested push events.

Response format: a list of PushEventDTO objects.

#### POST `/api/events/repeat_analysis`

Expects an optional JSON with analysis settings (regex to exclude branches).  
Returns a list of the found collaborations.

Input body format: an object of the form of an AnalysisSettingsDTO.  
Response format: a list of CollaborationDTO objects.

### Collaborations endpoint `/api/collaborations`

For all three routes, any of the provided url parameters are wrong (e.g., id of a project that doesn't exist),
the answer is 404 Not Found.

#### GET `/api/collaborations/project/{projectId}/pairs`

Returns the list of all collaborator pairs in a project.

Input param `projectId`: the id of a project (e.g., `87`).  
Response format: a list of CollaborationAuthorsDTO objects.  

#### GET `/api/collaborations/project/{projectId}/collaborators/{authorUsername}`

Returns the list of all collaborators of an author in a project.

Input param `projectId`: the id of a project (e.g., `87`).  
Input param `authorUsername`: the username of an author (e.g., `loving_booth`).  
Response format: a list of AuthorDTO objects.

#### GET `/api/collaborations/project/{projectId}/author1/{author1Username}/author2/{author2Username}`

Returns the list of all collaborations of two authors in a project.

Input param `projectId`: the id of a project (e.g., 87).  
Input param `author1Username`: the username of an author (e.g., `loving_booth`).  
Input param `author2Username`: the username of another author (e.g., `mystifying_williams`).  
Response format: a list of CollaborationDTO objects.

### Postman Collections

We provide a Postman Collection with example requests to the API.  
You can import the file `Collaboration_Analyzer.postman_collection.json` in Postman (v8.2.3).

The example requests are numbered and contain simple tests with a description of the expected responses.

## Tests

Run tests with: `./gradlew test`.  
Run tests with coverage with `./gradlew testCoverage`; this task fails if the coverage is below 80%. The Jacoco coverage
output can be found at `build/reports/jacoco.xml`. 

## Deployment

The GitLab CI pipeline has a release stage which builds a docker image.
The images are available in [this project's container registry](https://gitlab.com/aron.fiechter/collaboration-analyzer/container_registry).

You can run the latest build with:

`docker run -p 8080:8080 registry.gitlab.com/aron.fiechter/collaboration-analyzer:main`

To build and run the docker image locally, from the project root:

- build the JAR with `./gradlew build`
- build the docker image with `docker build . -t collaboration-analyzer-local`
- run the image with `docker run -p 8080:8080 collaboration-analyzer-local`
