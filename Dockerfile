FROM openjdk:11-slim

WORKDIR /collaboration-analyzer

ARG jarPath="build/libs/Collaboration Analyzer-0.0.1-SNAPSHOT.jar"
COPY ${jarPath} /collaboration-analyzer/app.jar

EXPOSE 8080
ENTRYPOINT ["java", "-jar", "app.jar"]
